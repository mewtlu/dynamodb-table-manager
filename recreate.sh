#!/bin/zsh

if [[ $1 == 'help' || -z "$1" ]]; then
  echo 'Usage:'
  echo 'recreate help - Outputs this help info'
  echo 'recreate delete <table name> - Deletes the given table name and outputs the definition to a file.'
  echo 'recreate create <table name> <table definition file path> - Creates the given table from the file definition created above'
  echo 'recreate recreate <table name> - Deletes the given table, then repeatedly attempts to recreate it (as deletion takes some time), sleeping 5 seconds between attempts, up to 5 attempts.'
  exit 1
fi

function modifyTable () {
  CREATE_ATTEMPT_DELAY=5
  TIME=$(date "+%s")
  TABLE_NAME=$2
  if [[ -z $3 ]]; then
    TABLE_JSON=$(aws dynamodb describe-table --table-name "$TABLE_NAME")
    if [[ -z $TABLE_JSON ]]; then
      echo 'Table not found, try again or pass in a definition file as the third parameter to this function.'
      return 4
    fi
    TABLE_FILE="dynamodb-table-definition-$2-$TIME.json"
    echo "$TABLE_JSON" > "$TABLE_FILE"
    echo "Outputted table definition to $TABLE_FILE"
  else
    TABLE_JSON=$(cat "$3")
  fi

  CreateOutputLogFile="dynamodb-table-create-out-$TIME.log"

  AttDef=$(echo "$TABLE_JSON" | jq ".Table.AttributeDefinitions")
  KeySchema=$(echo "$TABLE_JSON" | jq ".Table.KeySchema")
  BillingModeRaw=$(echo "$TABLE_JSON" | jq ".Table.BillingModeSummary.BillingMode")
  BillingMode=${BillingModeRaw//\"/}
  GlobalSecondaryIndexes=$(echo "$TABLE_JSON" | jq "[.Table.GlobalSecondaryIndexes[0] | { IndexName, KeySchema, Projection }]" )
  ProvisionedThroughput=$(echo "$TABLE_JSON" | jq ".Table.ProvisionedThroughput")
#  LocalSecondaryIndexes=$(echo "$TABLE_JSON" | jq ".Table.LocalSecondaryIndexes" )
#  ReadCapacityUnits=$(echo "$ProvisionedThroughput" | jq ".ReadCapacityUnits")
#  WriteCapacityUnits=$(echo "$ProvisionedThroughput" | jq ".WriteCapacityUnits")

  if [[ $1 == 'recreate' ]]; then
    modifyTable delete "$TABLE_NAME" "$TABLE_FILE"
    for i in {1..5}; do
      echo 'Attempting to create...'
      if modifyTable create "$TABLE_NAME" "$TABLE_FILE"; then
        echo 'Recreate successful!'
        return 0
      else
        echo "Failed to create, waiting ${CREATE_ATTEMPT_DELAY}s..."
        sleep $CREATE_ATTEMPT_DELAY
      fi
    done
  elif [[ $1 == 'delete' ]]; then
    DeleteOutputLogFile="dynamodb-table-delete-out-$TIME.log"
    echo 'Deleting table...'
    aws dynamodb delete-table \
      --table-name "$TABLE_NAME" > "$DeleteOutputLogFile"
    echo "Deleted table! Output can be found in $DeleteOutputLogFile"
  elif [[ $1 == 'create' ]]; then
    aws dynamodb create-table \
      --table-name "$TABLE_NAME" \
      --billing-mode="$BillingMode" \
      --attribute-definitions "$AttDef" \
      --key-schema "$KeySchema" \
      --global-secondary-indexes "$GlobalSecondaryIndexes" &> "$CreateOutputLogFile"
    #  --provisioned-throughput ReadCapacityUnits="$ReadCapacityUnits",WriteCapacityUnits="$WriteCapacityUnits"
    #  --local-secondary-indexes "$LocalSecondaryIndexes" \
    if [[ $? == 0 ]]; then
      echo "Created table! Output can be found in $CreateOutputLogFile"
    else
      echo "Error creating table: $(cat "$CreateOutputLogFile")"
      return 2
    fi
  else
    echo "Unknown command \"$1\""
    return 3
  fi
}

modifyTable "$1" "$2" "$3"
exit $?